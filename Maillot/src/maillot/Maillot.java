package maillot;

public class Maillot {
    
    private String manche;
    private String motif;
    private String couleurManche;
    private String couleurMotif;
    private Boolean select = false;

    public Maillot(String manche, String motif, String couleurManche, String couleurMotif) {
        this.manche = manche;
        this.motif = motif;
        this.couleurManche = couleurManche;
        this.couleurMotif = couleurMotif;
    }

    public String getManche() {
        return manche;
    }

    public String getMotif() {
        return motif;
    }

    public String getCouleurManche() {
        return couleurManche;
    }

    public String getCouleurMotif() {
        return couleurMotif;
    }
    
    public void setSelect(Boolean select) {
        this.select = select;
    }

    public Boolean getSelect() {
        return select;
    }
    
    public boolean egal(Maillot maillot){
        return this.manche.equals(maillot.manche)
               && this.motif.equals(maillot.motif)
               && this.couleurMotif.equals(maillot.couleurMotif)
               && this.couleurMotif.equals(maillot.couleurMotif);
    }
}
