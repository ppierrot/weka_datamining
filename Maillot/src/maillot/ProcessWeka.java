package maillot;

import java.util.ArrayList;

import weka.classifiers.trees.J48;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

public class ProcessWeka {
    
    public static String process(ArrayList<Maillot> maillots){
        
        //Definition du model
        FastVector maillot = new FastVector();
        FastVector couleurs = new FastVector();
        FastVector manche =  new FastVector();
        FastVector motif = new FastVector();
        FastVector select = new FastVector();
        
        couleurs.addElement("blanc");
        couleurs.addElement("bleu");
        couleurs.addElement("rouge");
        couleurs.addElement("vert");

        
        manche.addElement("court");
        manche.addElement("long");
        
        motif.addElement("horizontal");
        motif.addElement("vertical");
        
        select.addElement("oui");
        select.addElement("non");
        
        maillot.addElement(new Attribute("manche", manche));
        maillot.addElement(new Attribute("motif", motif));
        maillot.addElement(new Attribute("couleurManche", couleurs));
        maillot.addElement(new Attribute("couleurMotif", couleurs));
        maillot.addElement(new Attribute("selectionne", select));

        //Creation du DataSet 
        Instances dataset = new Instances("Maillots", maillot, maillots.size());
        dataset.setClassIndex(dataset.numAttributes() - 1);
        
        //Remplisssage du dataset
        for(Maillot m : maillots){
            double[] vals = new double[dataset.numAttributes()];
            vals[0] = manche.indexOf(m.getManche());
            vals[1] = motif.indexOf(m.getMotif());
            vals[2] = couleurs.indexOf(m.getCouleurManche());
            vals[3] = couleurs.indexOf(m.getCouleurMotif());
            vals[4] = m.getSelect() ? select.indexOf("oui") : select.indexOf("non");
            
            dataset.add(new Instance(1.0, vals));
        }
        System.out.println(dataset);
        
        String graph = "";
        try{
            J48 tree = new J48();
            tree.buildClassifier(dataset);
            graph = tree.graph();
            System.out.println(graph);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        finally{
            return graph;
        }
    }
    
}
