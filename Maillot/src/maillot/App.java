package maillot;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;

import java.awt.event.MouseAdapter;
import java.awt.image.BufferedImage;

import java.io.File;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.border.Border;

import weka.gui.treevisualizer.PlaceNode2;
import weka.gui.treevisualizer.TreeVisualizer;

public class App {
    JFrame jfMaillot;
    private JPanel pan = new JPanel(new GridLayout(5, 6));
    private JPanel mainPan = new JPanel();

    private JButton btnVal = new JButton("Valider la s�lection");
    ArrayList<JLabel> images = new ArrayList<JLabel>();

    public JPanel getMainPan() {
        return mainPan;
    }
    private Border redBorder = BorderFactory.createLineBorder(Color.RED, 2);
    private Border blackBorder = BorderFactory.createLineBorder(Color.BLACK, 0);
    
    private ArrayList<Maillot> maillots = new ArrayList<Maillot>();

    public ArrayList<Maillot> getMaillots() {
        return maillots;
    }

    public App(JFrame jfMaillot) {
        super();
        this.jfMaillot = jfMaillot;
        
        Random rand = new Random();
        HashMap<JLabel, Maillot> dessinExistant = new HashMap<JLabel, Maillot>();
        ArrayList<String> chainesComp = new ArrayList<>();
        
        btnVal.addMouseListener(new MouseAdapter(){
            public void mouseClicked(java.awt.event.MouseEvent e) {
                String graph = ProcessWeka.process(maillots);
                TreeVisualizer tv = new TreeVisualizer(null, graph, new PlaceNode2());
                JPanel treePan = new JPanel(new BorderLayout());
                treePan.add(tv, BorderLayout.CENTER);
                jfMaillot.setContentPane(treePan);
                jfMaillot.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                jfMaillot.setSize(1130, 460);
                jfMaillot.setResizable(false);
                jfMaillot.setLocationRelativeTo(null);
                jfMaillot.setVisible(true);
                tv.fitToScreen();
            }
        });
        
        for (int i = 0; i < 30; i++) {
            images.add(new JLabel());
        }
        
            for (JLabel jbl : images) {
                this.pan.add(jbl);
                jbl.addMouseListener(new java.awt.event.MouseAdapter() 
                {
                    public void mouseClicked(java.awt.event.MouseEvent e) {
                        Maillot maillotClique = dessinExistant.get(jbl);
                        maillotClique.setSelect(!maillotClique.getSelect());
                        if (jbl.getBorder() == redBorder) {
                           jbl.setBorder(blackBorder);
                       }
                       else {
                           jbl.setBorder(redBorder);
                       }
                    }
                });
            }
    
        for (int i = 0; i < 30; i++) {
            int manches = rand.nextInt(2);
            int motif = rand.nextInt(2);
            int couleurFond = rand.nextInt(4);
            int couleurMotif = rand.nextInt(4);
            
            //les attributs pour generer l'arbre
            String att_manche = "";
            String att_motif =  "";
            String att_couleurFond = "";
            String att_couleurMotif = "";
            
            /*while (couleurFond == couleurMotif) {
                couleurMotif = rand.nextInt(6);
            }*/
            String chaineComparative = Integer.toString(manches) + Integer.toString(motif) + Integer.toString(couleurFond) + Integer.toString(couleurMotif);
            BufferedImage img = new BufferedImage(150, 92, BufferedImage.TYPE_INT_ARGB);
            Graphics g = img.getGraphics();
            String chemin = ".\\images\\";
            g.drawImage((new ImageIcon(chemin + "corps\\court\\corps_court_blanc.png")).getImage(), 0, 0, null);
            switch(manches)
            {
            case 0:
                att_manche = "court";
                switch(couleurFond)
                {
                case 0:
                    g.drawImage((new ImageIcon(chemin + "corps\\court\\corps_court_blanc.png")).getImage(), 0, 0, null);
                    att_couleurFond = "blanc";
                    break;
                case 1:
                    g.drawImage((new ImageIcon(chemin + "corps\\court\\corps_court_bleu.png")).getImage(), 0, 0, null);
                    att_couleurFond = "bleu";
                    break;
                case 2:
                    g.drawImage((new ImageIcon(chemin + "corps\\court\\corps_court_rouge.png")).getImage(), 0, 0, null);
                    att_couleurFond = "rouge";
                    break;
                case 3:
                    g.drawImage((new ImageIcon(chemin + "corps\\court\\corps_court_vert.png")).getImage(), 0, 0, null);
                    att_couleurFond = "vert";
                    break;
                }
                break;
            case 1:
                att_manche = "long";
                switch(couleurFond) 
                {
                case 0:
                    g.drawImage((new ImageIcon(chemin + "corps\\long\\corps_long_blanc.png")).getImage(), 0, 0, null);
                    att_couleurFond = "blanc";
                    break;
                case 1:
                    g.drawImage((new ImageIcon(chemin + "corps\\long\\corps_long_bleu.png")).getImage(), 0, 0, null);
                    att_couleurFond = "bleu";
                    break;
                case 2:
                    g.drawImage((new ImageIcon(chemin + "corps\\long\\corps_long_rouge.png")).getImage(), 0, 0, null);
                    att_couleurFond = "rouge";
                    break;
                case 3:
                    g.drawImage((new ImageIcon(chemin + "corps\\long\\corps_long_vert.png")).getImage(), 0, 0, null);
                    att_couleurFond = "vert";
                    break;
                }
                break;
                
            }
            
            switch(motif) 
            {
            case 0:
                att_motif = "vertical";
                switch(couleurMotif) 
                {
                case 0:
                    g.drawImage((new ImageIcon(chemin + "motif\\horizontal\\motif_horizontal_blanc.png")).getImage(), 0, 0, null);
                    att_couleurMotif = "blanc";
                    break;
                case 1:
                    g.drawImage((new ImageIcon(chemin + "motif\\horizontal\\motif_horizontal_bleu.png")).getImage(), 0, 0, null);
                    att_couleurMotif = "bleu";
                    break;
                case 2:
                    g.drawImage((new ImageIcon(chemin + "motif\\horizontal\\motif_horizontal_rouge.png")).getImage(), 0, 0, null);
                    att_couleurMotif = "rouge";
                    break;
                case 3:
                    g.drawImage((new ImageIcon(chemin + "motif\\horizontal\\motif_horizontal_vert.png")).getImage(), 0, 0, null);
                    att_couleurMotif = "vert";
                    break;
                }
                break;
            case 1:
                att_motif = "horizontal";
                switch(couleurMotif) 
                {
                case 0:
                    g.drawImage((new ImageIcon(chemin + "motif\\vertical\\motif_vertical_blanc.png")).getImage(), 0, 0, null);
                    att_couleurMotif = "blanc";
                    break;
                case 1:
                    g.drawImage((new ImageIcon(chemin + "motif\\vertical\\motif_vertical_bleu.png")).getImage(), 0, 0, null);
                    att_couleurMotif = "bleu";
                    break;
                case 2:
                    g.drawImage((new ImageIcon(chemin + "motif\\vertical\\motif_vertical_rouge.png")).getImage(), 0, 0, null);
                    att_couleurMotif = "rouge";
                    break;
                case 3:
                    g.drawImage((new ImageIcon(chemin + "motif\\vertical\\motif_vertical_vert.png")).getImage(), 0, 0, null);
                    att_couleurMotif = "vert";
                    break;
                }
                break;
            }
            
           /*
            boolean existant = false;
            for(Map.Entry<JLabel, Maillot> entry : dessinExistant.entrySet()) {
                if (entry.getValue().egal(maillotCree)) {
                    existant = true;
                }
            } */
            
            
            System.out.println("etape suivante " + i);
            if (chainesComp.contains(chaineComparative)) {
                i--;
            }
            else {
                Maillot maillotCree = new Maillot (att_manche, att_motif, att_couleurFond, att_couleurMotif);
                dessinExistant.put(images.get(i), maillotCree);
                ImageIcon ic = new ImageIcon(img);
                images.get(i).setIcon(ic);
                chainesComp.add(chaineComparative);

                //cr�ation objet Maillot avec les attributs pour g�n�rer arbre

            }
            /*try {
                img = ImageIO.read(new File(chemin + "corps\\court\\corps_court_blanc.png"));
            } catch (IOException e) {
                System.out.println("Mauvaise image");
            }*/
            
        }
        
        for(Map.Entry<JLabel, Maillot> entry : dessinExistant.entrySet()) {
            getMaillots().add(entry.getValue());
        }
       
        this.getMainPan().add(pan);
        this.getMainPan().add(btnVal);
        jfMaillot.setContentPane(mainPan);
        jfMaillot.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jfMaillot.setSize(1130, 550);
        jfMaillot.setResizable(false);
        jfMaillot.setLocationRelativeTo(null);
        jfMaillot.setVisible(true);
        
    }

    public static void main(String[] args) {
        JFrame fenetreMaillot = new JFrame("Choix de Maillot");
        App maillot = new App(fenetreMaillot);
    }
}
